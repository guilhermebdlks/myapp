package br.com.guilherme.bridgelinks.provider;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import br.com.guilherme.bridgelinks.MainActivity;
import br.com.guilherme.bridgelinks.R;
import br.com.guilherme.bridgelinks.service.BdlkWidgetService;

/**
 * Created by Guilherme on 21/01/2016.
 */
public class BdlkWidgetProvider extends AppWidgetProvider {
    public static final String LOAD_BDLKS = "br.com.guilherme.bridgelinks.provider.LOAD_BDLKS";
    public static final String FILTER_BDLK = "br.com.guilherme.bridgelinks.provider.FILTER_BDLK";
    public static final String FILTER_BDLK_ITEM = "br.com.guilherme.bridgelinks.provider.FILTER_BDLK_ITEM";

    @Override
    public void onReceive(Context context, Intent intent) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

        if( intent != null){
            if(intent.getAction().equalsIgnoreCase(LOAD_BDLKS)){
                int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);

                if(appWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID){
                    //appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId, R.id.lv_collection);
                    onUpdate(context, appWidgetManager, new int[]{appWidgetId});
                }
            }
            else if(intent.getAction().equalsIgnoreCase(FILTER_BDLK)){
                String urlPhoto = intent.getStringExtra(FILTER_BDLK_ITEM);

                Intent it = new Intent(context, MainActivity.class);
                it.putExtra(FILTER_BDLK_ITEM, urlPhoto);
                it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(it);
            }
        }

        super.onReceive(context, intent);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        for(int i = 0; i< appWidgetIds.length; i++){
            Intent itService = new Intent(context, BdlkWidgetService.class);
            itService.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);




            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.appwidget_collection);
            views.setRemoteAdapter(R.id.lv_collection, itService);
            views.setEmptyView(R.id.lv_collection, R.id.tv_loading);

            Intent itLoadBdlks = new Intent(context, BdlkWidgetProvider.class);
            itLoadBdlks.setAction(LOAD_BDLKS);
            itLoadBdlks.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);
            PendingIntent piLoadBdlks = PendingIntent.getBroadcast(context, 0, itLoadBdlks, 0);
            views.setOnClickPendingIntent(R.id.iv_update_collection, piLoadBdlks);

            Intent itOpen = new Intent(context, MainActivity.class);
            PendingIntent piOpen = PendingIntent.getActivity(context, 0, itOpen, 0);
            views.setOnClickPendingIntent(R.id.iv_open_app, piOpen);

            Intent itFilterBdlk = new Intent(context, BdlkWidgetProvider.class);
            itFilterBdlk.setAction(FILTER_BDLK);
            PendingIntent piFilterBdlk = PendingIntent.getBroadcast(context, 0, itFilterBdlk, 0);
            views.setPendingIntentTemplate(R.id.lv_collection, piFilterBdlk);

            appWidgetManager.updateAppWidget(appWidgetIds[i], views);
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds[i], R.id.lv_collection);
        }

        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }
}
