package br.com.guilherme.bridgelinks.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import br.com.guilherme.bridgelinks.R;
import br.com.guilherme.bridgelinks.fragments.BdlkFragment;
import br.com.guilherme.bridgelinks.fragments.ImpressosBdlkFragment;
import br.com.guilherme.bridgelinks.fragments.SitesBdlkFragment;
import br.com.guilherme.bridgelinks.fragments.SobreBdlkFragment;
import br.com.guilherme.bridgelinks.fragments.SocialBdlkFragment;
import br.com.guilherme.bridgelinks.fragments.VisualBdlkFragment;

/**
 * Created by viniciusthiengo on 5/18/15.
 */
public class TabsAdapter extends FragmentPagerAdapter {
    private Context mContext;
    private String[] titles = {"TODOS", "SITES", "VISUAL", "SOCIAL", "IMPRESSOS", "SOBRE"};
    private int[] icons = new int[]{R.drawable.all, R.drawable.sites, R.drawable.visual, R.drawable.social, R.drawable.impressos, R.drawable.sobre};
    private int heightIcon;


    public TabsAdapter(FragmentManager fm, Context c) {
        super(fm);

        mContext = c;
        double scale = c.getResources().getDisplayMetrics().density;
        heightIcon = (int)( 24 * scale + 0.5f );
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag = null;

        if(position == 0){ // TODOS OS TRABALHOS
            frag = new BdlkFragment();
        }
        else if(position == 1){ // SITES
            frag = new SitesBdlkFragment();
        }
        else if(position == 2){ // VISUAL
            frag = new VisualBdlkFragment();
        }
        else if(position == 3){ // SOCIAL MIDIA
            frag = new SocialBdlkFragment();
        }
        else if(position == 4){ // IMPRESSOS
            frag = new ImpressosBdlkFragment();
        }
        else if(position == 5){ // SOBRE
            frag = new SobreBdlkFragment();
        }

        Bundle b = new Bundle();
        b.putInt("position", position);

        frag.setArguments(b);

        return frag;
    }

    @Override
    public int getCount() {
        return titles.length;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        /*Drawable d = mContext.getResources().getDrawable( icons[position] );
        d.setBounds(0, 0, heightIcon, heightIcon);

        ImageSpan is = new ImageSpan( d );

        SpannableString sp = new SpannableString(" ");
        sp.setSpan( is, 0, sp.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );


        return ( sp );*/
        return ( titles[position] );
    }
}
