package br.com.guilherme.bridgelinks.service;

import android.content.Intent;
import android.widget.RemoteViewsService;

import br.com.guilherme.bridgelinks.adapters.BdlkWidgetFactoryAdapter;

/**
 * Created by Guilherme on 21/01/2016.
 */
public class BdlkWidgetService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new BdlkWidgetFactoryAdapter(this, intent);
    }
}
