package br.com.guilherme.bridgelinks;

import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;


import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.android.gms.appinvite.AppInviteReferral;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.ArrayList;
import java.util.List;

import br.com.guilherme.bridgelinks.adapters.TabsAdapter;
import br.com.guilherme.bridgelinks.domain.Bdlk;
import br.com.guilherme.bridgelinks.domain.Person;
import br.com.guilherme.bridgelinks.extras.SlidingTabLayout;
import br.com.guilherme.bridgelinks.provider.BdlkWidgetProvider;
import de.greenrobot.event.EventBus;


public class MainActivity extends AppCompatActivity {
    private static String TAG = "LOG";

    private Toolbar mToolbar;
    private Drawer.Result navigationDrawerLeft;
    private AccountHeader.Result headerNavigationLeft;
    //private FloatingActionMenu fab;
    private int mItemDrawerSelected;
    private int mProfileDrawerSelected;

    private List<PrimaryDrawerItem> listCatefories;
    private List<Person> listProfile;
    private List<Bdlk> listBdlks;

    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;

    public static final int REQUEST_INVITE = 78;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TRANSITIONS
            if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ){
                /*Explode trans1 = new Explode();
                trans1.setDuration(3000);
                Fade trans2 = new Fade();
                trans2.setDuration(3000);

                getWindow().setExitTransition( trans1 );
                getWindow().setReenterTransition( trans2 );*/

                TransitionInflater inflater = TransitionInflater.from( this );
                Transition transition = inflater.inflateTransition( R.transition.transitions );

                getWindow().setSharedElementExitTransition( transition );
            }

        super.onCreate(savedInstanceState);

        Fresco.initialize(this);
        setContentView(R.layout.activity_main);

        if(savedInstanceState != null){
            mItemDrawerSelected = savedInstanceState.getInt("mItemDrawerSelected", 0);
            mProfileDrawerSelected = savedInstanceState.getInt("mProfileDrawerSelected", 0);
            listBdlks = savedInstanceState.getParcelableArrayList("listBdlks");
        }
        else{
            listBdlks = getSetCarList(50);

            if(AppInviteReferral.hasReferral(getIntent())){
                launchInviteCall(getIntent());
            }
        }

        // TOOLBAR
            mToolbar = (Toolbar) findViewById(R.id.tb_main);
            mToolbar.setTitle("");
            //mToolbar.setSubtitle("just a subtitle");
            mToolbar.setLogo(R.drawable.logo);
            setSupportActionBar(mToolbar);

            /*getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(false);*/


        // FRAGMENT
            /*Fragment frag = getSupportFragmentManager().findFragmentByTag("mainFrag");
            if(frag == null) {
                frag = new BdlkFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.rl_fragment_container, frag, "mainFrag");
                ft.commit();
            }*/


        // TABS
            mViewPager = (ViewPager) findViewById(R.id.vp_tabs);
            mViewPager.setAdapter(new TabsAdapter(getSupportFragmentManager(), this));

            mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.stl_tabs);
            //mSlidingTabLayout.setDistributeEvenly(true);
            mSlidingTabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.colorFAB));
            mSlidingTabLayout.setCustomTabView(R.layout.tab_view, R.id.tv_tab);
            mSlidingTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    navigationDrawerLeft.setSelection(position);
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
            mSlidingTabLayout.setViewPager(mViewPager);
            //mSlidingTabLayout.setHorizontalFadingEdgeEnabled(true);
            //mSlidingTabLayout.setHorizontalScrollBarEnabled(true);


        // NAVIGATION DRAWER
        // HEADER
        headerNavigationLeft = new AccountHeader()
                .withActivity(this)
                .withCompactStyle(false)
                .withSavedInstance(savedInstanceState)
                .withThreeSmallProfileImages(false)
                .withHeaderBackground(R.drawable.background3)

                .build();

        // BODY
        navigationDrawerLeft = new Drawer()
                .withActivity(this)
                .withToolbar(mToolbar)
                .withDisplayBelowToolbar(false)
                .withActionBarDrawerToggleAnimated(true)
                .withDrawerGravity(Gravity.START)
                .withSavedInstance(savedInstanceState)
                .withActionBarDrawerToggle(true)
                .withAccountHeader(headerNavigationLeft)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l, IDrawerItem iDrawerItem) {
                        mViewPager.setCurrentItem(i);

                            /*Fragment frag = null;
                            mItemDrawerSelected = i;

                            if(i == 0){ // ALL CARS
                                frag = new BdlkFragment();
                            }
                            else if(i == 1){ // LUXURY CAR
                                frag = new SitesBdlkFragment();
                            }
                            else if(i == 2){ // SPORT CAR
                                frag = new VisualBdlkFragment();
                            }
                            else if(i == 3){ // OLD CAR
                                frag = new SocialBdlkFragment();
                            }
                            else if(i == 4){ // POPULAR CAR
                                frag = new ImpressosBdlkFragment();
                            }

                            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.rl_fragment_container, frag, "mainFrag");
                            ft.commit();

                            mToolbar.setTitle( ((PrimaryDrawerItem) iDrawerItem).getName() );*/
                    }
                })
                .withOnDrawerItemLongClickListener(new Drawer.OnDrawerItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l, IDrawerItem iDrawerItem) {
                        Toast.makeText(MainActivity.this, "onItemLongClick: " + i, Toast.LENGTH_SHORT).show();
                        return false;
                    }
                })
                .build();

        listCatefories = getSetCategoryList();
        if(listCatefories != null && listCatefories.size() > 0){
            for( int i = 0; i < listCatefories.size(); i++ ){
                navigationDrawerLeft.addItem( listCatefories.get(i) );
            }
            navigationDrawerLeft.setSelection(mItemDrawerSelected);
        }


        // FLOATING ACTION BUTTON
        //fab = (FloatingActionMenu) findViewById(R.id.fab);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(getIntent() != null && getIntent().getStringExtra(BdlkWidgetProvider.FILTER_BDLK_ITEM) != null){
            Bdlk bdlk = new Bdlk();
            bdlk.setUrlPhoto(getIntent().getStringExtra(BdlkWidgetProvider.FILTER_BDLK_ITEM));
            setIntent(null);

            EventBus.getDefault().post(bdlk);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView;
        MenuItem item = menu.findItem(R.id.action_searchable_activity);

        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ){
            searchView = (SearchView) item.getActionView();
        }
        else{
            searchView = (SearchView) MenuItemCompat.getActionView( item );
        }

        searchView.setSearchableInfo( searchManager.getSearchableInfo( getComponentName() ) );
        searchView.setQueryHint( getResources().getString(R.string.search_hint) );

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        /*if(id == R.id.action_second_activity){
            startActivity(new Intent(this, SecondActivity.class));
        }
        else if(id == R.id.action_transition_activity){
            startActivity(new Intent(this, TransitionActivity_A.class));
        }*/
        return super.onOptionsItemSelected(item);
    }



    // CATEGORIES
    private List<PrimaryDrawerItem> getSetCategoryList(){
        String[] names = new String[]{"Todos os Trabalhos", "Sites", "Identidade Visual", "Mídia Social", "Impressos", "Sobre Nós"};
        int[] icons = new int[]{R.drawable.all, R.drawable.sites, R.drawable.visual, R.drawable.social, R.drawable.impressos, R.drawable.sobre};
        int[] iconsSelected = new int[]{R.drawable.all_selected, R.drawable.sites_selected, R.drawable.visual_selected, R.drawable.social_selected, R.drawable.impressos_selected, R.drawable.sobre_selected};
        List<PrimaryDrawerItem> list = new ArrayList<>();

        for(int i = 0; i < names.length; i++){
            PrimaryDrawerItem aux = new PrimaryDrawerItem();
            aux.setName( names[i] );
            aux.setIcon(getResources().getDrawable(icons[i]));
            aux.setTextColor(getResources().getColor(R.color.colorPrimarytext));
            aux.setSelectedIcon(getResources().getDrawable(iconsSelected[i]));
            aux.setSelectedTextColor(getResources().getColor(R.color.colorPrimary));

            list.add( aux );
        }
        return(list);
    }



    // BDLK
    public List<Bdlk> getSetCarList(int qtd){
        return(getSetCarList(qtd, 0));
    }

    public List<Bdlk> getSetCarList(int qtd, int category){
        String[] title = new String[]{"Armazém da Vida Suplementos", "Completo Alimentos Balanceados", "ZAK Advogados", "Plan Segurança do Trabalho", "Papets", "ESB Do Brasil", "Píon Educacional"};
        String[] subtitle = new String[]{"Site", "Site", "Site", "Identidade Visual", "Identidade Visual", "Mídia Social", "Impressos"};
        int[] categories = new int[]{1, 1, 1, 2, 2, 3, 4};
        String[] photos = new String[]{"http://bdlk.com.br/w600/logo_armazem.png", "http://bdlk.com.br/w600/logo_complemento.png", "http://bdlk.com.br/w600/logo_zak.jpg", "http://bdlk.com.br/w600/logo_plan.png", "http://bdlk.com.br/w600/logo_papets.png", "http://bdlk.com.br/w600/logo_esb.jpg", "http://bdlk.com.br/w600/logo_pion.png"};

        String[] urlPhotos = new String[]{"logo_armazem.png", "logo_complemento.png", "logo_zak.jpg", "logo_plan.png", "logo_papets.png", "logo_esb.jpg", "logo_pion.png"};
        //String[] urlPhotos = new String[]{"logo_armazem.png", "logo_complemento.png", "logo_zak.png", "logo_plan.png", "logo_papets.png", "logo_esb.png", "logo_pion.png"};

        //STRING COM A DESCRIÇÃO
        String description = "a Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.";
        String armazem = "O Armazém da Vida é uma marca gaúcha que tem como compromisso diário a comercialização de suplementos alimentares, vitaminas, minerais e alimentos funcionais. Hoje, a marca possui lojas distribuídas em Porto Alegre, Canoas, Gravataí e São Leopoldo.";
        String zak = "A Zak Advogados atua, principalmente, na área de Direito Bancário, atendendo aos anseios \n" +
                "do consumidor quanto à necessidade de revisão de contratos, ações indenizatórias e \n" +
                "contenciosas, sempre em defesa do consumidor. \n" +
                "O escritório, localizado em Porto Alegre, conta com profissionais experientes e altamente \n" +
                "qualificados para a atuação no âmbito judicial e extrajudicial, capazes de realizar uma \n" +
                "assessoria jurídica individualizada para cada cliente.\n" +
                "O principal desafio da BDLK no desenvolvimento e gerenciamento da página da Zak no \n" +
                "Facebook é divulgar serviços e notícias referentes ao mundo do direito bancário. Através de \n" +
                "uma linguagem simples e de fácil entendimento, buscamos sempre o esclarecimento do \n" +
                "público na área jurídica.";
        String plan = "A Plan Zero engenharia de segurança do trabalho conta com uma equipe experiente com sólida vivencia nas áreas arquitetura e engenharia, oferencendo os serviços adequando as normas regulamentadores nacionais e internacionais.";
        String papets = "Papets surgiu com o intuito de integrar a um ambiente familiar, um espaço limpo e agradável para todos – inclusive para os pets! Um jeito prático e eficaz de ajudar os nossos melhores amigos!";
        String esb = "A ESB do Brasil é uma empresa dedicada a produção, comercialização e serviços na área de envoltórios naturais (tripas). Com a missão de oferecer ferecer alternativas em tripa natural para embutidos, para solução de problemas e ou otimização dos resultados na produção de linguiças, beneficiando a tripa original do cliente e ou fornecendo tripa em tubing de alta performance, com pós venda e suporte técnico diferenciados.";
        String pion = "A Píon Educacional é uma empresa especializada em Grupos de Estudos preparatórios para Vestibulares e ENEM, Aulas Particulares e Grupos de Acompanhamento para disciplinas do Ensino Superior da UFRGS. ";
        String completo = "A Completo Alimentos criou um novo conceito em nutrição. Ela é a primeira empresa a nível mundial a pesquisar e produzir alimentos completos e balanceados do ponto de vista nutricional. Completos porque possuem todos os nutrientes que nosso organismo necessita, balanceados porque possuem estes nutrientes na proporção das nossas necessidades. Com isto, ela criou alimentos que vão à origem das nossas necessidades nutricionais.";

        List<Bdlk> listAux = new ArrayList<>();

        //AQUI É ONDE DEFINE O SITE DO LOCAL E A DESCRIÇÃO
        for(int i = 0; i < qtd; i++){
            Bdlk c = new Bdlk( title[i % title.length], subtitle[ i % subtitle.length], photos[i % title.length], Bdlk.PATH + urlPhotos[i % title.length] );
            if(c.getTitle() == "Armazém da Vida Suplementos"){
                c.setDescription(armazem);
                c.setSite("http://armazemdavida.com.br/");
                c.setTel("05133335588");
            } else if(c.getTitle() == "Plan Segurança do Trabalho") {
                c.setDescription(plan);
                c.setSite("http://www.planzero.com.br/capa/index.html");
                c.setTel("05133335588");
            }else if(c.getTitle() == "ESB Do Brasil") {
                c.setDescription(esb);
                c.setSite("http://esbdobrasil.com/");
                c.setTel("05133335588");
            }else if(c.getTitle() == "Píon Educacional") {
                c.setDescription(pion);
                c.setSite("http://pionedu.com.br/");
                c.setTel("05133335588");
            }else if(c.getTitle() == "Completo Alimentos Balanceados") {
                c.setDescription(completo);
                c.setSite("http://completoalimentos.com.br/");
                c.setTel("05133335588");
            }else if(c.getTitle() == "Papets") {
                c.setDescription(papets);
                c.setSite("http://papets.com.br/");
                c.setTel("05133335588");
            }else if(c.getTitle() == "ZAK Advogados") {
                c.setDescription(zak);
                c.setSite("http://zakadvogados.com.br/");
                c.setTel("05133335588");
            }else {
                c.setSite("https://www.facebook.com/BDLKS/");
                c.setDescription(description);
                c.setTel("05133335588");
            }
            c.setCategory(categories[i % subtitle.length]);


            if(category != 0 && c.getCategory() != category){
                continue;
            }

            listAux.add(c);
        }
        return(listAux);
    }

        /*public List<Bdlk> getSetCarList(int qtd, int category){
            String[] models = new String[]{"Gallardo", "Vyron", "Corvette", "Pagani Zonda", "Porsche 911 Carrera", "BMW 720i", "DB77", "Mustang", "Camaro", "CT6"};
            String[] brands = new String[]{"Lamborghini", " bugatti", "Chevrolet", "Pagani", "Porsche", "BMW", "Aston Martin", "Ford", "Chevrolet", "Cadillac"};
            int[] categories = new int[]{2, 1, 2, 1, 1, 4, 3, 2, 4, 1};
            int[] photos = new int[]{R.drawable.gallardo, R.drawable.vyron, R.drawable.corvette, R.drawable.paganni_zonda, R.drawable.porsche_911, R.drawable.bmw_720, R.drawable.db77, R.drawable.mustang, R.drawable.camaro, R.drawable.ct6};
            String[] urlPhotos = new String[]{"gallardo.jpg", "vyron.jpg", "corvette.jpg", "paganni_zonda.jpg", "porsche_911.jpg", "bmw_720.jpg", "db77.jpg", "mustang.jpg", "camaro.jpg", "ct6.jpg"};
            String description = "Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.";
            List<Bdlk> listAux = new ArrayList<>();

            for(int i = 0; i < qtd; i++){
                Bdlk c = new Bdlk( models[i % models.length], brands[ i % brands.length], photos[i % models.length], Bdlk.PATH + urlPhotos[i % models.length] );
                c.setDescription( description );
                c.setCategory( categories[ i % brands.length ] );
                c.setSite("33221155");

                if(category != 0 && c.getCategory() != category){
                    continue;
                }

                listAux.add(c);
            }
            return(listAux);
        }*/

    public List<Bdlk> getCarsByCategory(int category){
        List<Bdlk> listAux = new ArrayList<>();
        for(int i = 0; i < listBdlks.size() ; i++){
            if(category != 0 && listBdlks.get(i).getCategory() != category){
                continue;
            }

            listAux.add(listBdlks.get(i));
        }
        return(listAux);
    }

    public List<Bdlk> getListBdlks(){
        return(listBdlks);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("mItemDrawerSelected", mItemDrawerSelected);
        outState.putInt("mProfileDrawerSelected", mProfileDrawerSelected);
        outState.putParcelableArrayList("listBdlks", (ArrayList<Bdlk>) listBdlks);
        outState = navigationDrawerLeft.saveInstanceState(outState);
        outState = headerNavigationLeft.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if(navigationDrawerLeft.isDrawerOpen()){
            navigationDrawerLeft.closeDrawer();
        }
        /*else if(fab.isOpened()){
            fab.close(true);
        }*/
        else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerInviteReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterInviteReceiver();
    }

    // APP INVITE
    private BroadcastReceiver mInviteReceiver;

    private void registerInviteReceiver(){
        mInviteReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(AppInviteReferral.hasReferral(intent)){
                    launchInviteCall(intent);
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(mInviteReceiver, new IntentFilter("deepLink"));
    }

    private void unregisterInviteReceiver(){
        if(mInviteReceiver != null){
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mInviteReceiver);
        }
    }

    private void launchInviteCall(Intent intent){
        Intent it = new Intent(intent).setClass(this, BdlkActivity.class);

        for(int i = 0, tamI = listBdlks.size(); i < tamI; i++){
            if(intent.toString().indexOf(listBdlks.get(i).getSubtitle()) > -1
                    && intent.toString().indexOf(listBdlks.get(i).getTitle()) > -1){
                it.putExtra("car", listBdlks.get(i));
                break;
            }
        }
        startActivity(it);
    }



}
