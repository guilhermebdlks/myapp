package br.com.guilherme.bridgelinks.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by viniciusthiengo on 4/5/15.
 */
public class Bdlk implements Parcelable {
    public static final String PATH = "http://www.bdlk.com.br/__w-600__/";

    private String title;
    private String subtitle;
    private String description;
    private int category;
    private String site;
    private String tel;
    private String photo;
    private String urlPhoto;

    public Bdlk(){}
    public Bdlk(String m, String b, String p, String up){
        title = m;
        subtitle = b;
        photo = p;
        urlPhoto = up;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    // PARCELABLE
    public Bdlk(Parcel parcel){
        setTitle(parcel.readString());
        setSubtitle(parcel.readString());
        setDescription(parcel.readString());
        setCategory(parcel.readInt());
        setSite(parcel.readString());
        setTel(parcel.readString());
        setPhoto(parcel.readString());
        setUrlPhoto(parcel.readString());
    }
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString( getTitle() );
        dest.writeString( getSubtitle() );
        dest.writeString( getDescription() );
        dest.writeInt(getCategory());
        dest.writeString(getSite());
        dest.writeString( getTel() );
        dest.writeString(getPhoto());
        dest.writeString( getUrlPhoto() );
    }
    public static final Parcelable.Creator<Bdlk> CREATOR = new Parcelable.Creator<Bdlk>(){
        @Override
        public Bdlk createFromParcel(Parcel source) {
            return new Bdlk(source);
        }
        @Override
        public Bdlk[] newArray(int size) {
            return new Bdlk[size];
        }
    };


}