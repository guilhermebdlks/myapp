package br.com.guilherme.bridgelinks.fragments;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import br.com.guilherme.bridgelinks.R;

public class SobreBdlkFragment extends BdlkFragment {
        String html = "\n" +
                "\n" +
                "<html>\n" +
                "<head>\n" +
                " \n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "\n" +
                "  <div style=' \n" +
                "  \twidth: 100%;\n" +
                "    text-align: center;\n" +
                "    background: #FFFFFF;\n" +
                "    color: #000;\n" +
                "    '>\n" +
                "  <div style='    \n" +
                "  \twidth: 100%;\n" +
                "    text-align: center;'>\n" +
                "    <div style='\n" +
                "\t    max-width: 800px;\n" +
                "\t    display: inline-block;\n" +
                "\t    margin: 50px 0 80px 0;\n" +
                "\t    padding: 0 20px;'>\n" +
                "      <h1>Quem somos</h1>  \n" +
                "      <p>\n" +
                "        A Bridge Links é uma agência digital com sede em Porto Alegre que atua desde a criação da identidade visual da empresa até o gerenciamento de marca nas redes sociais. Também desenvolve aplicativos para iOS e Android, além de elaborar sites responsivos para levar sua marca aos clientes quando eles estiverem conectados pelo smartphone ou tablet. \n" +
                "      </p>\n" +
                "    </div>\n" +
                "  </div>\n" +
                "  \n" +
                "  <div style=\"   \n" +
                "  \twidth: 100%;\n" +
                "    text-align: center;\n" +
                "    background: #FFFFFF;\n" +
                "    color: #000;  \n" +
                "    background: #FFF;\n" +
                "    z-index: 10;\n" +
                "    position: relative;\n" +
                "    border-top: #000 solid 5px;\">\n" +
                "      <h1>Serviços</h1>\n" +
                "      <ul style=\" margin-left:-30px;\">\n" +
                "          <li style='display: inline-block;\n" +
                "      width: 280px;'>\n" +
                "            \n" +
                "            <h2>Identidade visual</h2>\n" +
                "            <p>O desenvolvimento de uma identidade visual forte &eacute; essencial para as empresas que querem se destacar no seu segmento de mercado. A equipe da ag&ecirc;ncia tem experi&ecirc;ncia na cria&ccedil;&atilde;o de identidades visuais que transmitem o conceito e o posicionamento das marcas aos consumidores.</p>\n" +
                "          </li>\n" +
                "          <li style=' \n" +
                "          display: inline-block;\n" +
                "\t    width: 280px;'>\n" +
                "            \n" +
                "            <h2>Sites</h2>\n" +
                "            <p>Desenvolvemos sites responsivos que se adaptam desde as telas dos smartphones at&eacute; a resolu&ccedil;&atilde;o das Smart TVs, tornando o acesso dos consumidores ao portal da sua empresa mais pr&aacute;tico e objetivo.</p>\n" +
                "          </li>\n" +
                "          <li style='display: inline-block;\n" +
                "    width: 280px;'>\n" +
                "          \n" +
                "            <h2>E-commerce</h2>\n" +
                "            <p>A ag&ecirc;ncia digital Bridge Links tem experi&ecirc;ncia na cria&ccedil;&atilde;o de lojas virtuais e desenvolvimento de sites de compra coletivas com sistemas totalmente personalizados para se adequarem &agrave;s necessidades da sua empresa.</p>\n" +
                "          </li>\n" +
                "          <li style='display: inline-block;\n" +
                "    width: 280px;'>\n" +
                "            \n" +
                "            <h2>Mobile</h2>\n" +
                "            <p>Al&eacute;m dos sites mobile-friendly, criamos aplicativos para Android e iOS que garantem que a sua marca continue em contato com os seus clientes durante o tempo no qual eles est&atilde;o conectados pelo smartphone.</p>\n" +
                "          </li>\n" +
                "          <li style='display: inline-block;\n" +
                "    width: 280px;'>\n" +
                "           \n" +
                "            <h2>Social Mídia</h2>\n" +
                "            <p>Criamos conte&uacute;do e geramos engajamento para que sua marca atinja o p&uacute;blico-alvo certo nas redes, divulgando seu servi&ccedil;o a consumidores em potencial e fidelizando seus atuais clientes.&nbsp;</p>\n" +
                "          </li>\n" +
                "      </ul>\n" +
                "    </div>\n" +
                "  </div>\n" +
                "  \n" +
                "</div>\n" +
                "\n" +
                "</body>\n" +
                "</html>";
        String mime = "text/html";
        String encoding = "utf-8";

        @Override
        public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);



        }


        @Override
        public View onCreateView(LayoutInflater inflater,
                                 ViewGroup container,
                                 Bundle savedInstanceState) {
                View view = inflater.inflate(R.layout.layout_fragment, container, false);

                mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_list);

                WebView wv = (WebView)view.findViewById(R.id.webView);
                wv.getSettings().setJavaScriptEnabled(true);
                wv.loadDataWithBaseURL(null, html, mime, encoding, null);




                return view;
        }


}
